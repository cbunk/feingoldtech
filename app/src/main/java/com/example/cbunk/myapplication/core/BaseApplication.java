package com.example.cbunk.myapplication.core;

import com.example.cbunk.myapplication.core.dagger.application.ApplicationComponent;
import com.example.cbunk.myapplication.core.dagger.application.ApplicationModule;
import com.example.cbunk.myapplication.core.dagger.application.DaggerApplicationComponent;

import android.app.Application;

public class BaseApplication extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        initDependencyInjection();

    }

    public ApplicationComponent getComponent() {
        return component;
    }

    private void initDependencyInjection() {
        DaggerApplicationComponent.Builder builder = DaggerApplicationComponent.builder().applicationModule(
                new ApplicationModule(this));

        component = builder.build();
        component.inject(this);
    }
}
