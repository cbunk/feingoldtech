package com.example.cbunk.myapplication.core;

import java.util.List;

import com.google.common.collect.Lists;

import android.support.v7.widget.RecyclerView;

/**
 * Created by cbunk on 15.12.15.
 */
public abstract class BaseAdapter<T> extends RecyclerView.Adapter {

    private List<T> list = Lists.newArrayList();

    public BaseAdapter() { }

    public BaseAdapter(final List<T> list) {
        this.list = list;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        onBindViewHolder(holder, position, list.get(position));
    }

    public abstract void onBindViewHolder(final RecyclerView.ViewHolder holder, int position, final T t);

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void add(final List<T> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void setList(final List<T> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
