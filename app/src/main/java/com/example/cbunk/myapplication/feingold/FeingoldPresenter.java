package com.example.cbunk.myapplication.feingold;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.example.cbunk.myapplication.core.LogUtils;
import com.example.cbunk.myapplication.core.dagger.activity.ActivityInstanceScope;
import com.example.cbunk.myapplication.core.espresso.EspressoIdlingResource;
import com.example.cbunk.myapplication.core.rxjava.ObservableHelper;
import com.example.cbunk.myapplication.feingold.view.ContactUiModel;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import rx.Observer;

@ActivityInstanceScope
public class FeingoldPresenter implements FeingoldContract.Presenter {

    private static final Logger LOG = LogUtils.logger(FeingoldPresenter.class);

    @Nullable
    private FeingoldContract.View view;

    private FeingoldContract.Model model;
    PhoneNumberUtil phoneNumberUtil;

    @Inject
    public FeingoldPresenter(final FeingoldContract.Model model, final PhoneNumberUtil phoneNumberUtil) {
        this.model = Preconditions.checkNotNull(model);
        this.phoneNumberUtil = phoneNumberUtil;
    }

    public void onViewCreated(final FeingoldContract.View view) {
        this.view = view;
    }

    public void onDestroyView() {
        this.view = null;
    }

    @Override
    public void findContact(final String phoneNumber) {
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            if (view != null) {
                view.showNotification("Not a valid Number");
            }

            return;
        }

        String e164Number = convertToE164(phoneNumber);

        if (e164Number == null) {
            if (view != null) {
                view.showNotification("Not a valid Number");
                return;
            }
        }

        ObservableHelper.prepare(model.findContact(e164Number)).subscribe(new Observer<List<ContactUiModel>>() {
                @Override
                public void onCompleted() {
                    EspressoIdlingResource.decrement();
                }

                @Override
                public void onError(final Throwable e) {
                    if (view != null) {
                        view.showNotification("No Contacts Found");
                        view.showContact(Lists.<ContactUiModel>newArrayList());
                    }

                    EspressoIdlingResource.decrement();
                }

                @Override
                public void onNext(final List<ContactUiModel> contactUiModels) {
                    if (view != null && !contactUiModels.isEmpty()) {
                        view.showContact(contactUiModels);
                    }
                }
            });

        EspressoIdlingResource.increment();

    }

    @Nullable
    @VisibleForTesting
    String convertToE164(final String phoneNumber) {
        Phonenumber.PhoneNumber number = null;
        try {
            number = phoneNumberUtil.parse(phoneNumber, Locale.getDefault().getCountry());
            return phoneNumberUtil.format(number, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (NumberParseException e) {
            e.printStackTrace();
            return null;
        }

    }

}
