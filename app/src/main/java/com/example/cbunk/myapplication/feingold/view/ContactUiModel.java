package com.example.cbunk.myapplication.feingold.view;

import com.google.common.base.Preconditions;

public class ContactUiModel {
    private String name;

    public ContactUiModel(final String name) {
        this.name = Preconditions.checkNotNull(name);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContactUiModel that = (ContactUiModel) o;

        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
