package com.example.cbunk.myapplication.feingold.view;

import com.example.cbunk.myapplication.core.BaseFragment;
import com.example.cbunk.myapplication.core.BaseFragmentActivity;
import com.example.cbunk.myapplication.core.dagger.activity.ActivityComponent;

public class FeingoldActivity extends BaseFragmentActivity {
    @Override
    protected void doInjection(final ActivityComponent component) {
        component.inject(this);
    }

    @Override
    protected BaseFragment contentFragment() {
        return new FeingoldFragment();
    }

}
