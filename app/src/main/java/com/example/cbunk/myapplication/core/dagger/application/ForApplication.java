package com.example.cbunk.myapplication.core.dagger.application;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;

/**
 * @author  Christian Bunk <christian.bunk@zalando.de>
 */
@Qualifier
@Retention(RUNTIME)
public @interface ForApplication { }
