package com.example.cbunk.myapplication.feingold.model;

import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.example.cbunk.myapplication.core.LogUtils;
import com.example.cbunk.myapplication.feingold.FeingoldContract;
import com.example.cbunk.myapplication.feingold.view.ContactUiModel;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import android.content.ContentResolver;

import android.database.Cursor;

import android.net.Uri;

import android.provider.BaseColumns;
import android.provider.ContactsContract;

import android.support.annotation.NonNull;

import io.rx_cache.DynamicKey;

import rx.Observable;
import rx.Subscriber;

public class FeingoldModel implements FeingoldContract.Model {

    public interface CacheContactProvider {
        Observable<List<ContactUiModel>> findContact(Observable<List<ContactUiModel>> observable,
                DynamicKey phoneNumber);
    }

    private static final Logger LOG = LogUtils.logger(FeingoldModel.class);

    private ContentResolver contentResolver;
    private final CacheContactProvider cacheContactProvider;

    @Inject
    public FeingoldModel(final ContentResolver contentResolver, final CacheContactProvider cacheContactProvider) {
        this.contentResolver = contentResolver;
        this.cacheContactProvider = cacheContactProvider;
    }

    @Override
    public Observable<List<ContactUiModel>> findContact(final String phoneNumber) {
        return cacheContactProvider.findContact(findContactsCall(phoneNumber), new DynamicKey(phoneNumber));
    }

    @NonNull
    private Observable<List<ContactUiModel>> findContactsCall(final String phoneNumber) {
        return Observable.create(new Observable.OnSubscribe<List<ContactUiModel>>() {
                    @Override
                    public void call(final Subscriber<? super List<ContactUiModel>> subscriber) {
                        if (phoneNumber == null || phoneNumber.isEmpty()) {
                            subscriber.onError(new Exception("No contacts found"));
                        }

                        Uri uri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI,
                                Uri.encode(phoneNumber));

                        Cursor contactLookup = contentResolver.query(uri,
                                new String[] {BaseColumns._ID, ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null,
                                null);

                        if (contactLookup == null) {
                            subscriber.onError(new Exception("No contacts found"));
                            return;
                        }

                        HashSet<ContactUiModel> contactUiModelHashSet = Sets.newHashSet();

                        while (contactLookup.moveToNext()) {
                            String name = contactLookup.getString(
                                    contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                            contactUiModelHashSet.add(new ContactUiModel(name));
                        }

                        contactLookup.close();

                        if (contactUiModelHashSet.isEmpty()) {
                            subscriber.onError(new Exception("No contacts found"));
                            return;
                        }

                        // String contactId = contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
                        List<ContactUiModel> contactUiModels = Lists.newArrayList(contactUiModelHashSet.iterator());
                        subscriber.onNext(contactUiModels);
                        subscriber.onCompleted();
                    }
                });
    }

}
