package com.example.cbunk.myapplication.feingold.view;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.BaseFragment;
import com.example.cbunk.myapplication.core.LogUtils;
import com.example.cbunk.myapplication.core.dagger.activity.ActivityComponent;
import com.example.cbunk.myapplication.feingold.FeingoldContract;
import com.example.cbunk.myapplication.feingold.FeingoldPresenter;

import android.os.Bundle;

import android.support.annotation.Nullable;

import android.support.design.widget.Snackbar;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.View;

import android.widget.EditText;

import butterknife.Bind;
import butterknife.OnClick;

public class FeingoldFragment extends BaseFragment implements FeingoldContract.View {

    private static final Logger LOG = LogUtils.logger(FeingoldFragment.class);

    @Bind(R.id.feingold_fragment_recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.feingold_fragment_phonenumber_edit_text)
    EditText phoneNumberEditText;

    @Inject
    FeingoldPresenter presenter;

    private ContactAdapter contactAdapter;

    @Nullable
    @Override
    protected Integer layoutToInflate() {
        return R.layout.feingold_fragment;
    }

    @Override
    protected void doInjection(final ActivityComponent component) {
        component.inject(this);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.onViewCreated(this);

        initRecyclerView();
    }

    private void initRecyclerView() {
        contactAdapter = new ContactAdapter();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(contactAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.onDestroyView();
    }

    @Override
    public void showContact(final List<ContactUiModel> contactUiModel) {
        contactAdapter.setList(contactUiModel);
    }

    @Override
    public void showNotification(final String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.feingold_fragment_find_button)
    public void onClickFind() {
        presenter.findContact(phoneNumberEditText.getText().toString());
    }
}
