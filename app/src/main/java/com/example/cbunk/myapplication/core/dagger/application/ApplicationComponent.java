package com.example.cbunk.myapplication.core.dagger.application;

import javax.inject.Singleton;

import com.example.cbunk.myapplication.core.BaseApplication;
import com.example.cbunk.myapplication.feingold.model.FeingoldModel;

import dagger.Component;

import io.rx_cache.internal.RxCache;

/**
 * Created by cbunk on 15.12.15.
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(BaseApplication application);

    RxCache rxCache();

    FeingoldModel.CacheContactProvider cacheContactProvider();
}
