package com.example.cbunk.myapplication.core.dagger.activity;

import com.example.cbunk.myapplication.core.BaseActivity;
import com.example.cbunk.myapplication.core.BaseApplication;
import com.example.cbunk.myapplication.feingold.FeingoldContract;
import com.example.cbunk.myapplication.feingold.model.FeingoldModel;

import com.google.i18n.phonenumbers.PhoneNumberUtil;

import android.app.ActionBar;

import android.content.ContentResolver;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(final BaseActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityInstanceScope
    public BaseApplication provideApplication() {
        return (BaseApplication) activity.getApplicationContext();
    }

    @Provides
    @ActivityInstanceScope
    public ActionBar provideActionBar() {
        return activity.getActionBar();
    }

    @Provides
    @ActivityInstanceScope
    public Context provideContext() {
        return activity;
    }

    @Provides
    @ActivityInstanceScope
    public FeingoldContract.Model provideFeindgoldModel(final ContentResolver contentResolver,
            final FeingoldModel.CacheContactProvider cacheContactProvider) {
        return new FeingoldModel(contentResolver, cacheContactProvider);
    }

    @Provides
    public ContentResolver provideContentResolver() {
        return activity.getContentResolver();
    }

// @Provides
// @ActivityInstanceScope
// public FeingoldPresenter provideFeingoldPresenter(final FeingoldContract.Model model, final Context context) {
// return new FeingoldPresenter(model, context);
// }

    @Provides
    @ActivityInstanceScope
    public PhoneNumberUtil providePhoneNumberUtil() {
        return PhoneNumberUtil.getInstance();
    }
}
