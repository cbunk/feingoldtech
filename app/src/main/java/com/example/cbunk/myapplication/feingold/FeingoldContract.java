package com.example.cbunk.myapplication.feingold;

import java.util.List;

import com.example.cbunk.myapplication.feingold.view.ContactUiModel;

import rx.Observable;

public interface FeingoldContract {

    interface Model {
        Observable<List<ContactUiModel>> findContact(String phoneNumber);
    }

    interface View {
        void showContact(List<ContactUiModel> contactUiModel);

        void showNotification(String message);
    }

    interface Presenter {
        void findContact(String phoneNumber);
    }
}
