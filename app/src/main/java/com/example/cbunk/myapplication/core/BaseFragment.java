package com.example.cbunk.myapplication.core;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.dagger.activity.ActivityComponent;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {
    @Nullable
// @Bind(R.id.toolbar)
    protected Toolbar toolbar;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        doInjection(createComponent());
        super.onCreate(savedInstanceState);
        setRetainInstance(true); // every fragments retains its instance, as long you don't have nested fragments and
                                 // the fragment is not in the back stack

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {

        final Integer layoutToInflate = layoutToInflate();
        if (layoutToInflate == null) {
            throw new IllegalArgumentException("layout must not be null!");
        }

        return inflater.inflate(layoutToInflate, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // do the view injection
        ButterKnife.bind(this, view);

        if (toolbar != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
            toolbar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.toolbar)); // generic rgb

            Integer titleRes = titleRes();
            if (titleRes != null) {
                toolbar.setTitle(titleRes);
            }

            String title = title();
            if (title != null) {
                toolbar.setTitle(title);
            }

            ActionBar actionBar = ((BaseActivity) getActivity()).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(showUpButton());
            }
        }
    }

    protected boolean showUpButton() {
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);
    }

    @Nullable
    protected abstract Integer layoutToInflate();

    protected void doInjection(final ActivityComponent component) { }

    private ActivityComponent createComponent() {
        return ((BaseActivity) getActivity()).getActivityComponent();
    }

    @Nullable
    @StringRes
    protected Integer titleRes() {
        return null;
    }

    @Nullable
    protected String title() {
        return null;
    }
}
