package com.example.cbunk.myapplication.feingold.view;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.BaseViewHolder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import butterknife.Bind;

public class ContactViewHolder extends BaseViewHolder<ContactUiModel> {

    @Bind(R.id.feingold_contact_item_name_textview)
    TextView nameTextView;

    public static ContactViewHolder create(final ViewGroup viewGroup) {
        return new ContactViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feingold_contact_item,
                    viewGroup, false));
    }

    private ContactViewHolder(final View itemView) {
        super(itemView);
    }

    @Override
    protected void bindData(final ContactUiModel contactUiModel, final int position) {
        nameTextView.setText(contactUiModel.getName());
    }
}
