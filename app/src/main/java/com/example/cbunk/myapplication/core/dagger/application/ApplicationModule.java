package com.example.cbunk.myapplication.core.dagger.application;

import java.io.File;

import javax.inject.Singleton;

import com.example.cbunk.myapplication.core.BaseApplication;
import com.example.cbunk.myapplication.feingold.model.FeingoldModel;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

import io.rx_cache.internal.RxCache;

/**
 * @author  Christian Bunk <christian.bunk@zalando.de>
 */
@Module
public class ApplicationModule {
    private final BaseApplication application;

    public ApplicationModule(final BaseApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    public RxCache provideRxCache(final Context context) {
        RxCache.Builder builder = new RxCache.Builder();

        File cacheDir = context.getFilesDir();

        return builder.persistence(cacheDir);
    }

    @Provides
    @Singleton
    public FeingoldModel.CacheContactProvider provideProvider(final RxCache rxCache) {
        return rxCache.using(FeingoldModel.CacheContactProvider.class);
    }
}
