package com.example.cbunk.myapplication.feingold.view;

import com.example.cbunk.myapplication.core.BaseAdapter;

import android.support.v7.widget.RecyclerView;

import android.view.ViewGroup;

public class ContactAdapter extends BaseAdapter<ContactUiModel> {
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position,
            final ContactUiModel contactUiModel) {
        if (holder instanceof ContactViewHolder) {
            ContactViewHolder contactViewHolder = (ContactViewHolder) holder;
            contactViewHolder.bindData(contactUiModel, position);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return ContactViewHolder.create(parent);
    }
}
