package com.example.cbunk.myapplication.feingold;

import static org.mockito.Matchers.eq;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.cbunk.myapplication.feingold.view.ContactUiModel;

import com.google.common.collect.Lists;

import com.google.i18n.phonenumbers.PhoneNumberUtil;

import rx.Observable;
import rx.Scheduler;

import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;

import rx.schedulers.Schedulers;

public class FeingoldPresenterTest {
    public static final String NO_CONTACTS_FOUND = "No Contacts Found";
    public static final String NOT_A_VALID_NUMBER = "Not a valid Number";
    public static final String NULL = null;
    public static final String EMPTY = "";

    @Mock
    private FeingoldContract.Model model;

    @Mock
    private FeingoldContract.View view;

    @Mock
    private ContactUiModel contactUiModel;

    @Mock
    private PhoneNumberUtil phoneNumberUtils;

    private FeingoldPresenter feingoldPresenter;
    public static final String PHONE_NUMBER = "0171 12345678";
    public static final String PHONE_NUMBER_E164 = "+49 0171 12345678";

    @Before
    public void setupWeatherPresenter() {

        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
                @Override
                public Scheduler getMainThreadScheduler() {
                    return Schedulers.immediate();
                }
            });

        feingoldPresenter = spy(new FeingoldPresenter(model, phoneNumberUtils));
        feingoldPresenter.onViewCreated(view);

    }

    @After
    public void tearDown() {
        RxAndroidPlugins.getInstance().reset();
    }

    @Test
    public void testFindContact() {
        List<ContactUiModel> contactUiModels = Lists.newArrayList(contactUiModel);
        when(feingoldPresenter.convertToE164(PHONE_NUMBER)).thenReturn(PHONE_NUMBER_E164);
        when(model.findContact(PHONE_NUMBER_E164)).thenReturn(Observable.just(contactUiModels));

        feingoldPresenter.findContact(PHONE_NUMBER_E164);

        verify(model).findContact(eq(PHONE_NUMBER_E164));
        verify(view).showContact(contactUiModels);
    }

    @Test
    public void testFindContact_Should_ShowNotification_When_ObservableReturnsError() {
        when(feingoldPresenter.convertToE164(PHONE_NUMBER)).thenReturn(PHONE_NUMBER_E164);
        when(model.findContact(PHONE_NUMBER_E164)).thenReturn(Observable.<List<ContactUiModel>>error(
                new Exception(NO_CONTACTS_FOUND)));

        feingoldPresenter.findContact(PHONE_NUMBER_E164);

        verify(model).findContact(eq(PHONE_NUMBER_E164));
        verify(view).showNotification(NO_CONTACTS_FOUND);
    }

    @Test
    public void testFindContact_Should_ShowNotification_When_PhoneNumberIsNull() {
        feingoldPresenter.findContact(NULL);

        verify(view).showNotification(NOT_A_VALID_NUMBER);
    }

    @Test
    public void testFindContact_Should_ShowNotification_When_PhoneNumberIsEmpty() {
        feingoldPresenter.findContact(EMPTY);

        verify(view).showNotification(NOT_A_VALID_NUMBER);
    }

}
