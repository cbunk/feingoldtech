package com.example.cbunk.myapplication.feingold;

import static org.hamcrest.Matchers.allOf;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.feingold.view.FeingoldActivity;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;

public class FeingoldActivityTest {

    /**
     * {@link ActivityTestRule} is a JUnit {@link Rule @Rule} to launch your activity under test.
     *
     * <p>Rules are interceptors which are executed for each test method and are important building blocks of Junit
     * tests.
     */
    @Rule
    public ActivityTestRule<FeingoldActivity> feingoldActivityTestRule = new ActivityTestRule<>(FeingoldActivity.class);

    @Before
    public void registerIdlingResource() {
        Espresso.registerIdlingResources(feingoldActivityTestRule.getActivity().getCountingIdlingResource());
    }

    @After
    public void unregisterIdlingResource() {
        Espresso.unregisterIdlingResources(feingoldActivityTestRule.getActivity().getCountingIdlingResource());
    }

    @Test
    public void findPhoneNumber_Should_DisplayViews() {

        // Check if the add note screen is displayed
        onView(withId(R.id.feingold_fragment_recycler_view)).check(matches(isDisplayed()));
        onView(withId(R.id.feingold_fragment_find_button)).check(matches(isDisplayed()));
        onView(withId(R.id.feingold_fragment_phonenumber_edit_text)).check(matches(isDisplayed()));
    }

    @Test
    public void findPhoneNumber_Should_ShowSnackbar_When_EnterAlphabeticalCharacters() {
        onView(withId(R.id.feingold_fragment_phonenumber_edit_text)).perform(click()).perform(typeText("ertz"));
        onView(withId(R.id.feingold_fragment_find_button)).perform(click());

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText("Not a valid Number"))).check(matches(
                isDisplayed()));

        onView(withId(R.id.feingold_fragment_find_button)).perform(click());

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText("Not a valid Number"))).check(matches(
                isDisplayed()));
    }

}
